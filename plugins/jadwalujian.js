let fetch = require('node-fetch')

let handler = async(m, { conn, args, usedPrefix, command }) => {


    let res = await fetch(global.API('https://api.lppgurindam.com/jadwalujian.php'))
    let mes = await res.json()

    m.reply(mes.jadwal)



}

handler.help = ['jadwalujian']
handler.tags = ['cek']
handler.group = true
handler.fail = null
handler.command = /^jadwalujian$/i
module.exports = handler